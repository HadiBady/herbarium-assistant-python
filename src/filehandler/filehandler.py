# Developed by Hadi Badawi
# The purpose of this file is to handle any importing of cvs files for the
# Western Herbarium Labs

import in_gifts
import in_loans
import out_gifts
import out_loans

class FileMang:

	def __init__(self):

		self._Dictionary = {}
		self._catalogue = {}
		self._ID = 0


	def openFile(self, filename):
		try:
			contFile =open(continentFileName, "r")

			contFile.readline()
			c = 0
			line = contFile.readline()
			while line != "":
				self._catalogue[tempList2[0]]= (tempObject)
				self._cDictionary[tempList[0]] = tempList[1]
				line = contFile.readline()
				line2 = dataFile.readline()
		except FileNotFoundError:
			print("Error: File(s) are found.")
		finally:
			contFile.close()
			dataFile.close()

	def inGifts(file):
		tempList = []

		line = file.readline()

		while line != "":

			line = line.rstrip()
			tempList = line.split(",") # need to replace this with something that only separates by commas if the character before and after is a ""
			tempList = quotes_remover(tempList)

			tempgift = in_gifts(self._ID, templist[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6]. tempList[7])
			self._ID += 1

			file.readline()



id, request, loan_code, loan_status, acr_of_inst, institute,
		person_of_contact, institute_address, date_requested, invoice_date,
		loan_due_date, num_specimens_received, date_returned,
		instution_loan_closed_date, specs_returned,
		sub_balance_for_multi_return, balance, under_study,
		 outstanding, spec_desc, notes, comments, paperwork_found, digitized)

	def inLoans(file, request):
		tempList = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = line.split(",")

			tempList = quotes_remover(tempList)
			tempgift = in_gifts(self._ID, templist[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6]. tempList[7])
			self._ID += 1

			file.readline()


self, id, gift_num,  institution, contact,
	address, date_sent, date_recv, spec_num,
	spec_desc, notes, paperwork, digitized

	def outGifts():
		tempList = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = line.split(",")

			tempList = quotes_remover(tempList)

			tempgift = in_gifts(self._ID, templist[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6]. tempList[7])
			self._ID += 1

			file.readline()


 id, request, loan_code, loan_status, acr_of_inst, institute,
		person_of_contact, institute_address, date_requested,sent_date,
		loan_due_date, num_specimens_received, date_returned,
		instution_loan_closed_date, specs_returned,
		sub_balance_for_multi_return, balance, under_study,
		outstanding, spec_desc, for_study_by, herb_stack_check,
		notes_comments, paperwork_found, digitized

	def outLoans():
		tempList = []
		line = file.readline()
		while line != "":
			line = line.rstrip()
			tempList = line.split(",")

			tempList = quotes_remover(tempList)

			tempgift = in_gifts(self._ID, templist[0], tempList[1], tempList[2], tempList[3], tempList[4], tempList[5], tempList[6]. tempList[7])
			self._ID += 1

			file.readline()

	## a function to split a string by the comma, but only commas that have double quotes before and after the comma
	def comma_delimiter(string):
		c = string.length()
		i = 1
		lastsplit = 0
		tokens = []
		while (i < c):
			if (i + 1 == c):
				tokens.add(string[lastsplit: c])
			else if (string[i-1] == "\"" and string[i] == "," and string [i+1] == "\""):
				tokens.add(string[lastsplit:i-1])
				lastsplit = i+1
			i +=1

	## a function to remove the quotes at the beginning and end of the string, as CSV files add quotes as well as commas to separate items
	def quotes_remover(list):
		c = 0
		tempList = list
		while (c < tempList.length()):
			tempList[c].rstrip("\"")
			tempList[c].lstrip("\"")
			c +=1
		return tempList

	## this function will save the stuff to a file with the specified output name
	def saveInventoryCatalogue(self, filename):
		file = open(filename, "w")
		file.write("Name|Continent|Population|PopulationDensity\n")
		i = 0
		for c in self._catalogue:
			if len(self._catalogue) > 0:
				toWrite = str(self._catalogue[c].getName() + "|" + self._catalogue[c].getContinent() + "|" +
				str(self._catalogue[c].getPopulation()) + "|" + "%.2f" %(self._catalogue[c].getPopDensity()) + "\n")
				file.write(toWrite)
				i += 1
			file.close()
		return i
