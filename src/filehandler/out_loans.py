\
class out_loans(object):
	def __init__(self, id, request, loan_code, loan_status, acr_of_inst, institute, 
		person_of_contact, institute_address, date_requested,sent_date, 
		loan_due_date, num_specimens_received, date_returned, 
		instution_loan_closed_date, specs_returned, 
		sub_balance_for_multi_return, balance, under_study,
		outstanding, spec_desc, for_study_by, herb_stack_check, 
		notes_comments, paperwork_found, digitized):
		self.id = id
		self.request = request
		self.loan_code = loan_code
		self.loan_status = laon_status
		self.acr_of_inst = acr_of_inst
		self.institute = institute
		self.person_of_contact = person_of_contact
		self.institute_address = institute_address
		self.invoice_data = invoice_data
		self.loan_due_date = loan_due_date
		self.num_specimens_received = num_specimens_received
		self.date_returned = data_returned
		self.institution_loan_closed_date = institution_loan_closed_date
		self.specs_returned = specs_returned
		self.sub_balance_for_multi_return = sub_balance_for_multi_return
		self.balance = balance
		self.under_study = under_study
		self.outstanding = outstanding
		self.spec_desc = spec_desc
		self.notes = notes
		self.comments = comments
		self.paperwork_found = paperwork_found
		self.digitized = digitized


	def __init__(self):
		self.id = 0
		self.request = ""
		self.loan_status = ""
		self.acr_of_inst = ""
		self.institute = ""
		self.person_of_contact = ""
		self.institute_address = ""
		self.invoice_data = ""
		self.loan_due_date = ""
		self.num_specimens_received = ""
		self.date_returned = ""
		self.institution_loan_closed_date = ""
		self.specs_returned = ""
		self.sub_balance_for_multi_return = ""
		self.balance = ""
		self.under_study = ""
		self.outstanding = ""
		self.spec_desc = ""
		self.comments = ""
		self.paperwork_found = ""
		self.digitized = ""

	def get_ID(self):
		return self.id
	def set_ID(self, id):
                self.id = id

	def get_loan_status(self):
                return self.loan_status
	def set_loan_status(self, status):
                self.loan_status=status

	def get_acr_of_inst(self):
                return self.acr_of_inst
	def set_acr_of_inst(self, acr):
                self.acr_of_inst = acr

	def get_institute(self):
                return self.institute
	def set_institute(self, inst):
                self.instittue = inst

	def get_person_of_contact(self):
                return self.person_of_contact
	def set_person_of_contact(self, contact):
                self.person_of_contact = contact

	def get_institute_address(self):
                return self.institute_address
	def set_institue_address(self, addr):
                self.institute_addres = addr

	def get_invoice_data(self):
                return self.invoice_data
	def set_invoice_data(self, invdat):
                self.invoice_data = invdat

	def get_loan_due_date(self):
                return self.loan_due_date
	def set_loan_due_date(self, due):
                self.loan_due_date = due

	def get_num_specimens_received(self):
                return self.num_specimens_received
	def set_num_specimens_received(self, rec):
                self.num_specimens_received = rec

	def get_date_returned(self):
                return self.date_returned
	def set_date_returned(self, retu):
        	self.date_returned = retu

	def get_institution_loan_closed_date(self):
                return self.institution_loan_closed_date
	def set_institution_loan_closed_date(self, closed):
                self.institution_loan_closed_date = closed

	def get_specs_returned(self):
                return self.specs_returned
	def set_specs_returned(self, specs):
                self.specs_returned = specs

	def get_sub_balance_for_multi_return(self):
                return self.sub_balance_for_multi_return
	def set_sub_balance_for_multi_return(self, sub):
                self.self.sub_balance_for_multi_return = sub 

	def get_balance(self):
                return self.balance
	def set_balance(self, bal):
                self.balance = balance

	def get_under_study(self):
                return self.under_study
	def set_under_study(self, study):
                self.under_study = study

	def get_outstanding(self):
                return self.outstanding
	def set_outstanding(self, remain):
                self.outstanding = remain

	def get_spec_desc(self):
                return self.spec_desc
	def set_spec_desc(self, desc):
                self.spec_desc = desc

	def get_comments(self):
                return self.comments
	def set_comments(self, str):
                self.comments = str

	def get_paperwork_found(self):
                return self.paperwork_found
	def set_paperwork_found(self, work):
                self.paperwork_found = work

	def get_digitized(self):
                return self.digitized
	def set_digitized(self, digi):
                self.digitized = digi

