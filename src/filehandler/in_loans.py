
class out_loans(object):
	def __init__(self, id, request, loan_code, loan_status, acr_of_inst, institute, 
		person_of_contact, institute_address, date_requested, invoice_date, 
		loan_due_date, num_specimens_received, date_returned, 
		instution_loan_closed_date, specs_returned, 
		sub_balance_for_multi_return, balance, under_study,
		 outstanding, spec_desc, notes, comments, paperwork_found, digitized):
		self._id = id
		self._request = request
        self._loan_code = loan_code
		self._loan_status = laon_status
		self._acr_of_inst = acr_of_inst
		self._institute = institute
		self._person_of_contact = person_of_contact
		self._institute_address = institute_address
		self._invoice_data = invoice_data
		self._loan_due_date = loan_due_date
		self._num_specimens_received = num_specimens_received
		self._date_returned = data_returned
		self._institution_loan_closed_date = institution_loan_closed_date
		self._specs_returned = specs_returned
		self._sub_balance_for_multi_return = sub_balance_for_multi_return
		self._balance = balance
		self._under_study = under_study
		self._outstanding = outstanding
		self._spec_desc = spec_desc
		self._notes = notes
		self._comments = comments
		self._paperwork_found = paperwork_found
		self._digitized = digitized


	def __init__(self):
		self._id = 0
		self._request = ""
		self._loan_status = ""
		self._acr_of_inst = ""
		self._institute = ""
		self._person_of_contact = ""
		self._institute_address = ""
		self._invoice_data = ""
		self._loan_due_date = ""
		self._num_specimens_received = ""
		self._date_returned = ""
		self._institution_loan_closed_date = ""
		self._specs_returned = ""
		self._sub_balance_for_multi_return = ""
		self._balance = ""
		self._under_study = ""
		self._outstanding = ""
		self._spec_desc = ""
		self._notes = ""
		self._comments = ""
		self._paperwork_found = ""
		self._digitized = ""

	def get_ID(self):
		return self._id
	def set_ID(self, id):
                self._id = id

	def get_loan_status(self):
                return self._loan_status
	def set_loan_status(self, status):
                self._loan_status=status

	def get_acr_of_inst(self):
                return self._acr_of_inst
	def set_acr_of_inst(self, acr):
                self._acr_of_inst = acr

	def get_institute(self):
                return self._institute
	def set_institute(self, inst):
                self._instittue = inst

	def get_person_of_contact(self):
                return self._person_of_contact
	def set_person_of_contact(self, contact):
                self._person_of_contact = contact

	def get_institute_address(self):
                return self._institute_address
	def set_institue_address(self, addr):
                self._institute_addres = addr

	def get_invoice_data(self):
                return self._invoice_data
	def set_invoice_data(self, invdat):
                self._invoice_data = invdat

	def get_loan_due_date(self):
                return self._loan_due_date
	def set_loan_due_date(self, due):
                self._loan_due_date = due

	def get_num_specimens_received(self):
                return self._num_specimens_received
	def set_num_specimens_received(self, rec):
                self._num_specimens_received = rec

	def get_date_returned(self):
                return self._date_returned
	def set_date_returned(self, retu):
        	self._date_returned = retu

	def get_institution_loan_closed_date(self):
                return self._institution_loan_closed_date
	def set_institution_loan_closed_date(self, closed):
                self._institution_loan_closed_date = closed

	def get_specs_returned(self):
                return self._specs_returned
	def set_specs_returned(self, specs):
                self._specs_returned = specs

	def get_sub_balance_for_multi_return(self):
                return self._sub_balance_for_multi_return
	def set_sub_balance_for_multi_return(self, sub):
                self._self.sub_balance_for_multi_return = sub 

	def get_balance(self):
                return self._balance
	def set_balance(self, bal):
                self._balance = balance

	def get_under_study(self):
                return self._under_study
	def set_under_study(self, study):
                self._under_study = study

	def get_outstanding(self):
                return self._outstanding
	def set_outstanding(self, remain):
                self._outstanding = remain

	def get_spec_desc(self):
                return self._spec_desc
	def set_spec_desc(self, desc):
                self._spec_desc = desc

	def get_comments(self):
                return self._comments
	def set_comments(self, str):
                self._comments = str

	def get_paperwork_found(self):
                return self._paperwork_found
	def set_paperwork_found(self, work):
                self._paperwork_found = work

	def get_digitized(self):
                return self._digitized
	def set_digitized(self, digi):
                self._digitized = digi

